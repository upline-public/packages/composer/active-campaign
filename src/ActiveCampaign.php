<?php

namespace Uplinestudio\ActiveCampaign;

use Uplinestudio\ActiveCampaign\Model\Contacts\Contact;
use Uplinestudio\ActiveCampaign\Model\Contacts\ContactTag;
use Uplinestudio\ActiveCampaign\Model\Contacts\Dto\ContactDto;
use Uplinestudio\ActiveCampaign\Model\Contacts\Dto\ContactTagDto;
use Uplinestudio\ActiveCampaign\Model\Dto\TagDto;
use Uplinestudio\ActiveCampaign\Model\ECommerce\Customer;
use Uplinestudio\ActiveCampaign\Model\ECommerce\Dto\CustomerDto;
use Uplinestudio\ActiveCampaign\Model\ECommerce\Dto\OrderDto;
use Uplinestudio\ActiveCampaign\Model\ECommerce\Order;
use Uplinestudio\ActiveCampaign\Model\Tag;

class ActiveCampaign
{
    public function __construct(string $url, string $token)
    {
        Config::init($url, $token);
    }

    public function saveContact(ContactDto $contactDto): array
    {
        $contactModel = new Contact();

        return $contactModel->findOrCreate($contactDto);
    }

    /**
     * @param int $contactId
     * @param array $tags = ['Opencart', 'Poland']
     */
    public function saveContactTags(int $contactId, array $tags): void
    {
        $contactTagModel = new ContactTag();
        $tagModel = new Tag();

        foreach ($tags as $tag) {
            $tagDto = new TagDto($tag, $tag);
            $tag = $tagModel->findOrCreate($tagDto);

            $contactTagDto = new ContactTagDto($contactId, (int)$tag['id']);
            $contactTagModel->create($contactTagDto);
        }
    }

    public function deleteContactTag(int $contactId, string $tagName)
    {
        $contactTagModel = new ContactTag();
        $contactModel = new Contact();
        $tagModel = new Tag();

        $tagToDelete = $tagModel->findOrCreate(new TagDto($tagName, $tagName));
        $contactTags = $contactModel->getTags($contactId);

        $contactTagToDelete = null;
        foreach ($contactTags as $contactTag) {
            if ((int)$contactTag['tag'] === (int)$tagToDelete['id']) {
                $contactTagToDelete = $contactTag;
            }
        }

        if (is_null($contactTagToDelete)) {
            return;
        }

        $contactTagModel->delete((int)$contactTagToDelete['id']);
    }

    public function saveCustomer(CustomerDto $customerDto): array
    {
        $customer = new Customer();

        return $customer->findOrCreate($customerDto);
    }

    public function saveOrder(OrderDto $orderDto): array
    {
        $order = new Order();

        return $order->create($orderDto);
    }
}
