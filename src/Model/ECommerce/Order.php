<?php

namespace Uplinestudio\ActiveCampaign\Model\ECommerce;

use Error;
use Uplinestudio\ActiveCampaign\Config;
use Uplinestudio\ActiveCampaign\Model\ECommerce\Dto\OrderDto;
use Uplinestudio\ActiveCampaign\Request;

class Order
{
    const URL = "ecomOrders";

    public function create(OrderDto $orderDto): array
    {
        $request = new Request(self::URL);

        $post = json_encode([
            "ecomOrder" => [
                "externalid" => $orderDto->getExternalId(),
                "source" => 1, // 0 - historical, 1 - real-time
                "email" => $orderDto->getEmail(),
                "orderProducts" => $orderDto->getOrderProducts(),
                "externalCreatedDate" => $orderDto->getExternalCreatedDate(),
                "totalPrice" => $orderDto->getTotalPrice(),
//                "shippingAmount" => 200,
//                "taxAmount" => 500,
//                "discountAmount" => 100,
                "currency" => $orderDto->getCurrency(),
                "connectionid" => Config::getConnectionId(),
                "customerid" => $orderDto->getCustomerid(),
            ]
        ]);

        $response = $request->setCustomRequest("POST")->setPostFields($post)->exec();

        $arrData = $response->getDataArray();

        if (!isset($arrData['ecomOrder'])) {
            throw new Error(("Ecom order not exist in response! " . print_r($arrData, true)));
        }

        $ecomOrder = $arrData['ecomOrder'];

        if (empty($ecomOrder)) {
            throw new Error("Ecom order is empty! " . print_r($arrData, true));
        }

        return $ecomOrder;
    }

//    private function delete(string $id): void
//    {
//        $request = new Request(self::URL . "/$id");
//
//        $request->setCustomRequest("DELETE")->exec();
//    }
}
