<?php

namespace Uplinestudio\ActiveCampaign\Model\Contacts;

use Error;
use Uplinestudio\ActiveCampaign\Logger;
use Uplinestudio\ActiveCampaign\Model\Contacts\Dto\ContactDto;
use Uplinestudio\ActiveCampaign\Query;
use Uplinestudio\ActiveCampaign\Request;

class Contact
{
    const URL = "contacts";

    public function create(ContactDto $contactDto): array
    {
        $request = new Request(self::URL);

        $response = $request->setCustomRequest("POST")->setPostFields(json_encode([
            "contact" => [
                "email" => $contactDto->getEmail(),
                "firstName" => $contactDto->getFirstname(),
                "lastName" => $contactDto->getLastname(),
                "phone" => $contactDto->getTelephone(),
            ]
        ]))->exec();

        $responseArray = $response->getDataArray();

        if (!isset($responseArray['contact'])) {
            Logger::write('Contact create error', $responseArray);
            throw new Error("No contact data in response of contact creation");
        }

        return $responseArray['contact'];
    }

    public function getTags(int $id)
    {
        $request = new Request(self::URL . "/$id/contactTags");

        $response = $request->setCustomRequest("GET")->exec();

        $responseArr = $response->getDataArray();

        if (!isset($responseArr['contactTags'])) {
            return [];
        }

        return $responseArr['contactTags'];
    }

    public function findByEmail(string $email): array
    {
        $filterQuery = Query::fromArray([
            "email" => $email
        ]);

        $request = new Request(self::URL . $filterQuery);

        $response = $request->setCustomRequest("GET")->exec();

        return $response->getDataArray();
    }

    public function findOrCreate(ContactDto $contactDto)
    {
        $customers = $this->findByEmail($contactDto->getEmail());

        if ((int)$customers['meta']['total'] > 0) {
            return $customers[self::URL][0];
        }

        return $this->create($contactDto);
    }

//    public function delete(string $id): void
//    {
//        $request = new Request(self::URL . "/$id");
//
//        $request->setCustomRequest("DELETE")->exec();
//    }
}
