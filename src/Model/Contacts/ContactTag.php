<?php

namespace Uplinestudio\ActiveCampaign\Model\Contacts;

use Uplinestudio\ActiveCampaign\Logger;
use Uplinestudio\ActiveCampaign\Model\Contacts\Dto\ContactTagDto;
use Uplinestudio\ActiveCampaign\Request;

class ContactTag
{
    const URL = "contactTags";

    public function create(ContactTagDto $contactTagDto): array
    {
        $request = new Request(self::URL);

        $response = $request->setCustomRequest("POST")->setPostFields(json_encode([
            "contactTag" => [
                "contact" => $contactTagDto->getContactId(),
                "tag" => $contactTagDto->getTagId(),
            ]
        ]))->exec();

        $responseArr = $response->getDataArray();

        if (empty($responseArr['contactTag'])) {
            Logger::write("Empty contactTag in response", $responseArr);
            return [];
        }

        return $response->getDataArray()['contactTag'];
    }

    public function delete(int $id)
    {
        $request = new Request(self::URL . "/$id");

        $request->setCustomRequest("DELETE")->exec();
    }
}
