<?php

namespace Uplinestudio\ActiveCampaign;

use Error;

class Response
{
    /**
     * @var mixed
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    public function getDataArray(): array
    {
        $dataArray = json_decode($this->data, true);

        if (!is_array($dataArray)) {
            Logger::write("Not array in response", $this->data);
            throw new Error("Response data must be array.");
        }

        return $dataArray;
    }
}
