<?php

namespace Uplinestudio\ActiveCampaign;

class Logger
{
    static private string $path = '/../storage/logs/';

    static function write(string $message, $data = [])
    {
        $stream = fopen(dirname(__FILE__) . self::$path . "error." . date('Y-m-d') . ".log", 'a');

        $text = date('H:i:s') . ' ERROR - ' . $message;

        if (!empty($data)) {
            $text .= "\n" . print_r($data, true);
        }

        $text .= "\n";

        fwrite($stream, $text);
    }
}
