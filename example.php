<?php

require "vendor/autoload.php";

use Uplinestudio\ActiveCampaign\ActiveCampaign;
use Uplinestudio\ActiveCampaign\Logger;
use Uplinestudio\ActiveCampaign\Model\Contacts\Dto\ContactDto;

Logger::write('test', ['some data']);

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$activeCampaign = new ActiveCampaign($_ENV["URL"], $_ENV["TOKEN"]);

$contactDto = new ContactDto('test@test.test', 'testfirstname', 'testlastname', 'testphone');
$contact = $activeCampaign->saveContact($contactDto);

$activeCampaign->saveContactTags((int)$contact['id'], ['Opencart', 'Poland']);
